<?php

class simple_calculator
{
    public $num_1;
    public $num_2;


    public function setInput($input1 = '', $input2 = '')
    {
        $this->num_1 = $input1;
        $this->num_2 = $input2;
    }

    public function add()
    {
        $data = $this->num_1 + $this->num_2;
        return $data;
    }

    public function sub()
    {
        $data = $this->num_1 - $this->num_2;
        return $data;
    }

    public function mul()
    {
        $data = $this->num_1 * $this->num_2;
        return $data;
    }

    public function div()
    {
        $data = $this->num_1 / $this->num_2;
        return $data;
    }
}

$numberInput = new simple_calculator();
$numberInput->setInput($_POST['num_1'], $_POST['num_2']);
if(!empty($_POST['num_1']) and !empty($_POST['num_2']) ) {

    if (isset($_POST['add'])) {
        echo $numberInput->add();
    }

    if (isset($_POST['sub'])) {
        echo $numberInput->sub();
    }

    if (isset($_POST['mul'])) {
        echo $numberInput->mul();
    }

    if (isset($_POST['div'])) {
        echo $numberInput->div();
    }
}
else {
    echo "Give me two number";
}