<?php


class GradeScale
{
 public $mathInput;
 public $engInput;
 public $bangInput;
 public $phyInput;
public function __construct($mathInput, $engInput, $bangInput, $phyInput)
{
    $this->mathInput = $mathInput;
    $this->engInput = $engInput;
    $this->bangInput = $bangInput;
    $this->phyInput = $phyInput;
}

    public function getTotalMarks()
 {
     $marks =  $this->mathInput + $this->engInput + $this->bangInput + $this->phyInput;
     return $marks;
 }
 public function grade()
 {
     if($this->mathInput<70 || $this->engInput<70 && $this->bangInput<70 && $this->phyInput){
         echo "Grade: Fail";
     }
     elseif ($this->mathInput<80 || $this->engInput<80 && $this->bangInput<80 && $this->phyInput){
         echo "Grade: A+";
     }
     elseif ($this->mathInput<90 || $this->engInput<90 && $this->bangInput<90 && $this->phyInput){
         echo "Grade: Golden A+";
     }
 }
}
$result1 = new GradeScale($_POST['math'],$_POST['eng'],$_POST['bang'],$_POST['phy']);
echo "Total Marks";
echo $result1->totalMarks()."<br>";
echo $result1->grade()
